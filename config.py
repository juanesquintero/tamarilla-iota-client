'''
The main config file for the appliaction,
contains all the important constants for configuration and to make it work just fine
'''
import logging
import os
import csv
from datetime import datetime

base_dir = os.path.abspath(os.path.dirname(__file__))

APP_NAME = 'TAmarilla DLT'
APP_LOGO = '/static/img/logo.png'
APP_FAVICON = '/static/img/favicon.png'
CURRENT_YEAR = datetime.now().year

# Reloadn on Jinja templates change
TEMPLATES_AUTO_RELOAD = True
DEBUG_TB_ENABLED = False

# Session Encryption key
SECRET_KEY = os.environ.get('SECRET_KEY')

# Folders to save uploads
UPLOAD_URL = '/app/static/uploads/'
UPLOAD_FOLDER = base_dir + UPLOAD_URL
IMG_UPLOAD_FOLDER = base_dir + UPLOAD_URL + '/images'

# SQLALCHEMY DB Config
# SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(base_dir, 'db/app.db')
SQLALCHEMY_DATABASE_URI = 'mssql+pyodbc://SA:Tamarilla1@localhost/Tamarilla?driver=ODBC Driver 17 for SQL SERVER'
SQLALCHEMY_TRACK_MODIFICATIONS = False

# Custom env vars
API_PATH = os.environ.get('API_PATH')
ADDRESS = os.environ.get('ADDRESS')

# CONSTANTS
CONSTANTS = dict()

'''LOGGING CONFIG'''
LOG_FORMAT = '%(asctime)s:%(levelname)s:%(name)s:%(message)s'
LOG_DIR = base_dir + '/logs'

# GENERAL LOGS (ALL)
logging.getLogger().setLevel(logging.DEBUG)
logging.basicConfig(filename=LOG_DIR+'/GENERALS.log',level=logging.DEBUG,format=LOG_FORMAT)

# ERROR LOGS
error_logger = logging.getLogger('error_logger')
error_logger.setLevel(logging.ERROR)
file_handler = logging.FileHandler(LOG_DIR+'/ERRORS.log')
file_handler.setFormatter(logging.Formatter(LOG_FORMAT))
error_logger.addHandler(file_handler)

# TANGLE STORAGE
if os.stat('tangle.csv').st_size == 0:
    with open('tangle.csv', 'w') as csvfile:
        fieldnames = ['radicado', 'tail_transaction_hash']
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writeheader()

'''END LOGGING CONFIG'''
