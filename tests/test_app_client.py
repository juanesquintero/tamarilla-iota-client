import flask.globals
from flask_unittest import ClientTestCase

from app import create_app
from .mocks import mock_register_form, mock_login_form


class TestAppClient(ClientTestCase):

    app = create_app()

    def config_app(self, current_app):
        current_app.config['TESTING'] = True
        current_app.config['PRESERVE_CONTEXT_ON_EXCEPTION'] = False
        current_app.config['WTF_CSRF_ENABLED'] = False

        # Import app dependencies
        from app import DB

        # Save values you will need on the test cases
        self.models = dict()
        self.app_db = DB


    def setUp(self, client):
        with self.app.app_context():
            self.config_app(self.app)


    def tearDown(self, client):
        pass

    def test_1_auth_routes(self, client):
        self.assertStatus(client.get('/login'), 200)
        self.assertStatus(client.get('/register'), 200)
        self.assertStatus(client.get('/logout'), 401)

    def test_2_register_form(self, client):
        form  =  mock_register_form(self.app)
        res = client.post('/register', data=form.data, follow_redirects=False)
        self.assertStatus(res, 201)

    def test_3_login_form(self, client):
        form  =  mock_login_form(self.app)
        res = client.post('/login', data=form.data)
        status = str(res._status_code)
        self.assertEqual(status[0], '2') # 2xx
