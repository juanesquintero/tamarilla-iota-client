from flask import flash
from app.models import Card
from app.utils.constants import MESSAGES
from app.utils.mixins import insert_row_from_form, update_record


def create_card(card_object):
    # Create and save user
    saved = insert_row_from_form(Card, card_object)
    if saved:
        flash(MESSAGES['DB_SUCCSESS_CREATION'], 'success')
        return True
    else:
        flash(MESSAGES['DB_FAILED_CREATION'], 'danger')
        return False

def select_card(radicado):
    transaction = Card().query.filter_by(radicado=radicado).first()
    if transaction:
        flash(MESSAGES['DB_SUCCSESS_SELECTION'], 'success')
        return transaction
    else:
        flash(MESSAGES['DB_FAILED_SELECTION'], 'danger')
        return None
    