import os
import csv
import json
import pandas

from flask import flash

def set_cards_log(id_register, tail_transaction_hash):
    path_file = os.getcwd()+'/tangle.csv'
    with open(path_file, 'a') as csvfile:
        fieldnames = ['radicado', 'tail_transaction_hash']
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writerow({
            'radicado': id_register,
            'tail_transaction_hash': tail_transaction_hash
        })


def get_cards_log():
    path_file = os.getcwd()+'/tangle.csv'
    tangle_log_pandas = pandas.read_csv(path_file)
    tangle_log_jsons = json.loads(tangle_log_pandas.to_json(orient='records'))
    return tangle_log_jsons
