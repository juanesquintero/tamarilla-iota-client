from app import DB as db

class Card(db.Model):

    radicado = db.Column(db.String(50), unique=True, nullable=False, primary_key=True)
    
    placa_vehiculo = db.Column(db.String(50), unique=False, nullable=False)
    tarjeta_vehiculo = db.Column(db.String(50), unique=False, nullable=False)

    numero_tarjeta_operacion = db.Column(db.String(100), unique=False, nullable=False)
    fecha_vencimiento = db.Column(db.String(50), unique=False, nullable=False)
    fecha_inicio_vigencia = db.Column(db.String(50), unique=False, nullable=False)
    fecha_fin_vigencia = db.Column(db.String(50), unique=False, nullable=False)

    tipo_documento_conductor = db.Column(db.String(50), unique=False, nullable=False)
    documento_conductor = db.Column(db.String(50), unique=False, nullable=False)
    nombre_conductor = db.Column(db.String(200), unique=False, nullable=False)
    licencia_conductor = db.Column(db.String(200), unique=False, nullable=False)
