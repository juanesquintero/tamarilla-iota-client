from flask import Blueprint, render_template, flash, session, current_app, request, redirect

Home = Blueprint('home', __name__)


@Home.route('/', methods=('GET', 'POST'))
def index():
    return render_template('index.html')
