import secrets
from random import randint

from flask import Blueprint, render_template, flash, session, current_app, request, redirect, url_for

from app.logic.dag_transaction import read_card, write_card
from app.logic.log_cards import set_cards_log, get_cards_log
from app.logic.db_card import create_card, select_card
from app.utils.constants import dni_types

Card = Blueprint('card', __name__)

APP_CONFIG = current_app.config
iota_api = APP_CONFIG['API_CLIENT']
iota_address = APP_CONFIG['ADDRESS']


@Card.route('/registry', methods=['GET', 'POST'])
def registry():
    form = request.form.to_dict()
    method = request.method
    if method == 'POST' and form:
        return registry_transaction(form)
    else:
        radicado = secrets.token_hex(nbytes=randint(10, 12))
        return render_template(
            'registry.html',
            radicado=radicado.upper(),
            dni_types=dni_types
        )


def registry_transaction(form):
    write = write_card(iota_api, iota_address, form)
    create = create_card(form)
    if write and create:
        set_cards_log(form['radicado'], write)
    
    return redirect(url_for('card.explorer'))



@Card.route('/explorer', methods=['GET'])
def explorer():
    tangle_log_list = get_cards_log()
    return render_template('explorer.html', transaction_list=tangle_log_list)


@Card.route('/detail/<query_type>/<identifier>', methods=['GET'])
def detail(query_type, identifier):
    card = {}
    tail_transaction_hash = None

    if query_type == 'tangle':
        tail_transaction_hash = identifier
        card = read_card(iota_api, tail_transaction_hash)
    elif query_type == 'db':
        radicado = identifier
        card = select_card(radicado)

    return render_template('detail.html', card=card, tail_transaction_hash=tail_transaction_hash)
