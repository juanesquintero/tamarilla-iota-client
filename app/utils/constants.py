
MESSAGES = dict(
    TANGLE_SUCCESS_WRITING='Registro guardado en el IOTA Tangle!',
    TANGLE_FAIL_WRITING='Error escribiendo el registro en el IOTA Tangle',
    
    TANGLE_SUCCESS_READING='Tarjeta obtenida de <b>IOTA DLT</b>',
    TANGLE_FAIL_READING='Error leyendo el registro en el IOTA Tangle',
    
    DB_SUCCSESS_CREATION='Registro creado en la base de datos!',
    DB_FAILED_CREATION='Error creando el registro en la base de datos!',

    DB_SUCCSESS_SELECTION='Registro obtenido de la <b>base de datos</b>',
    DB_FAILED_SELECTION='No se pudo recuperar el regidtro de la base de datos!',
)


dni_types = [
'Cédula de Ciudadanía',
'Cédula de Extranjeria',
'Pasaporte',
'Registro Civil',
'Tarjeta de Identidad',
]
