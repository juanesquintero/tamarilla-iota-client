'''
Initial Module for start and set up the Flask Appliaction
'''
import logging
from flask import Flask, render_template
from werkzeug.exceptions import HTTPException
from flask_sqlalchemy import SQLAlchemy
from iota import Iota

error_logger = logging.getLogger('error_logger')
DB = None

def create_app():
    # Instantiate app.
    app = Flask(__name__)

    with app.app_context():
        # Set app configuration
        config(app)

        # Define app db
        set_db(app)

        # Define API CLient
        api_client(app)

        # Register app routes.
        register_routes(app)

        # Handle app errors
        handle_errors(app)

        global DB
        DB.create_all()

    return app


def config(app):
    # Define app config
    app.config.from_object('config')


def set_db(app):
    global DB
    DB = SQLAlchemy(app)
    DB.init_app(app)

def api_client(app):
    api = Iota(app.config['API_PATH'], testnet=True)
    app.config['API_CLIENT'] = api


def register_routes(app):
    # Import views as blueprints
    from .views import Home, Card

    # Register blueprints.
    app.register_blueprint(Home)
    app.register_blueprint(Card, url_prefix='/cards')

def handle_errors(app):
    # Error handlers.
    error_template = 'error.html'

    @app.errorhandler(HTTPException)
    def handle_http_error(e):
        error_logger.error('EXCEPTION: '+str(e), exc_info=True)
        return render_template(error_template, error=e), e.code

    @app.errorhandler(Exception)
    def handle_exception(e):
        error_logger.error('EXCEPTION: '+str(e), exc_info=True)
        err = {
            'code': 500, 
            'name': 'Internal Server Error',
            'description': 'An Error has occurred on the application server, you might contact the administrator for more information'
        }
        return render_template(error_template, error=err, exception=True), 500
